package com.algorithm.hashmap;

/**
 * This is hashmap implemented in time complexity O(1)
 * @param <K>
 * @param <V>
 */
public class MyHashMap<K, V> {

    private Entry<K, V>[] buckets;
    private static int CAPACITY = 1 << 2; //16 1向左移了4位
    private int size = 0;

    public MyHashMap() {
        this(CAPACITY);
    }

    public MyHashMap(int size) {
        this.buckets = new Entry[size];
    }

    public void put(K key, V value) {
        Entry<K, V> newEntry = new Entry<>(key, value, null);
        int location = getHash(key) % getBucketSize();
        Entry<K, V> existing = buckets[location];
        //check if value already there
        if (existing == null) {
            buckets[location] = newEntry;
        } else {
            if (existing.key.equals(key)) {
                existing.value = value;
                return;
            }
            while (existing.next!=null){
                if (existing.next.key.equals(key)){
                    existing.next.value = value;
                    return;
                } else {
                    existing = existing.next;
                }
            }
            existing.next = newEntry;
        }
        size++;
    }


    public V get(K key) {
        int location = getHash(key) % getBucketSize();
        Entry<K, V> existing = buckets[location];
        //need to check if has more than 1 entry
        while (existing != null) {
            if (existing.key.equals(key)) {
                return existing.value;
            }
            existing = existing.next;
        }
        return null;
    }

    static final int getHash(Object key) {
        return key.hashCode();
    }

    public int getBucketSize() {
        return buckets.length;//it's 16 even if there is no value.
    }

    public int getSize(){
        return this.size;
    }

    public static void main(String[] args) {
        int i = 1 << 2;
        System.out.println(i);
    }

    private class Entry<K, V> {
        final K key;
        V value;
        Entry<K, V> next;

        public Entry(K key, V value, Entry<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
