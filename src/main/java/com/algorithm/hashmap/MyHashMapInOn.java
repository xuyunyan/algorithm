package com.algorithm.hashmap;

import java.util.ArrayList;
import java.util.List;

/**
 * This is hashmap implemented in time complexity O(n)
 * @param <K>
 * @param <V>
 */
public class MyHashMapInOn<K, V> {
    
    List<Entry> list = new ArrayList<Entry>();

    public void put(K key, V value){
        int keyCode = key.hashCode();
        Entry<K,V> entry = new Entry<K,V>(key, value);
        for (int i = 0; i < list.size(); i++){
            Entry<K,V> temp = list.get(i);
            if (temp.key.hashCode()==keyCode){
                list.remove(temp);
            }
        }
        list.add(entry);
    }


    public V get(K key){
        int hashCode= key.hashCode();
        for (int i=0; i<list.size(); i++){
            Entry<K,V> entry = list.get(i);
            if (entry.key.hashCode()==hashCode){
                return entry.value;
            }
        }
        return null;
    }

    static final int getHash(Object key) {
        return key.hashCode();
    }

    public static void main(String[] args) {
        int i = 1 << 2;
        System.out.println(i);
    }

    private class Entry<K, V> {
        final K key;
        V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
