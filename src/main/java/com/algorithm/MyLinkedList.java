package com.algorithm;

import org.junit.Test;

public class MyLinkedList {
    private Node head;
    private Node last;
    private int size;

    /**
     * get element
     * @param index
     * Time complexity: O(n)
     */
    public Node get(int index) throws Exception {
        if (index<0 || index>size){
            throw new IndexOutOfBoundsException("Out of range!");
        }
        Node temp = head;
        for (int i=0; i<index; i++){
            temp = temp.next;
        }
        return temp;
    }
    /**
     * insert element
     * @param data
     * @param index
     * Time complexity: O(1)
     */
    public void insert(int data, int index) throws Exception {
        if (index<0 || index>size){
            throw new IndexOutOfBoundsException("Out of range!");
        }
        Node insertedNode = new Node(data);
        if (size == 0){
            //insert head
            head = insertedNode;
            last = insertedNode;
        } else if (size == index){
            //insert tail
            last.next = insertedNode;
            last = insertedNode;
        } else {
            //insert in middle
            Node prevNode = get(index-1);
            Node nextNode = prevNode.next;
            prevNode.next = insertedNode;
            insertedNode.next = nextNode;
        }
        size++;
    }

    /**
     * delete element
     * @param index
     * Time complexity: O(1)
     */
    public Node remove(int index) throws Exception {
        if (index<0 || index>size){
            throw new IndexOutOfBoundsException("Out of range!");
        }
        Node removedNode = null;
        if (index == 0) {
            //delete head
            removedNode = head;
            head = head.next;
        } else if (index == size-1) {
            //delete last one
            removedNode = last;
            Node prevNode = get(index-1);
            prevNode.next = null;
        } else {
            //delete middle one
            Node prevNode = get(index-1);
            removedNode = prevNode.next;
            prevNode.next = prevNode.next.next;
        }
        size--;
        return removedNode;
    }

    /**
     * linked list node
     */
    private static class Node {
        int data;
        Node next;
        Node(int data) {
            this.data = data;
        }
    }

    /**
     * print the linked list
     * @throws Exception
     */
    public void output(){
        Node temp = head;
        while (temp!=null){
            System.out.println(temp.data);
            temp = temp.next;
        }
    }

}
