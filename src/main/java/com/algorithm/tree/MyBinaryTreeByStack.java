package com.algorithm.tree;

        import com.sun.source.tree.Tree;

        import java.util.Arrays;
        import java.util.LinkedList;
        import java.util.Stack;

public class MyBinaryTreeByStack {
    /**
     * create binary tree
     * @param inputList
     */
    public static TreeNode createBinaryTree(LinkedList<Integer> inputList){
        TreeNode node = null;
        if (inputList==null || inputList.isEmpty()){
            return null;
        }
        Integer data = inputList.removeFirst();
        if (data != null){
            node = new TreeNode(data);
            node.leftChild = createBinaryTree(inputList);
            node.rightChild = createBinaryTree(inputList);
        }
        return node;
    }

    /**
     * pre order traveral: root, left, right
     * @param root
     */
    public static void preOrderTraveral(TreeNode root){
        Stack<TreeNode> stack = new Stack<>();
        TreeNode treeNode = root;
        while (treeNode!=null || !stack.isEmpty()){
            //visit left child, and push into stack
            while (treeNode != null){
                System.out.println(treeNode.data);
                stack.push(treeNode);
                treeNode = treeNode.leftChild;
            }
            //if the node doesn't have right child, back to stack top, and visit right child
            if (!stack.isEmpty()){
                treeNode = stack.pop();
                treeNode = treeNode.rightChild;
            }
        }
    }

    /**
     * in order traveral: left, root, right
     * @param root
     */
    public static void inOrderTraveral(TreeNode root){
        Stack<TreeNode> stack = new Stack<>();
        TreeNode treeNode = root;
        while (treeNode!=null || !stack.isEmpty()){
            //visit left child, and push into stack
            while (treeNode != null){
                stack.push(treeNode);
                treeNode = treeNode.leftChild;
            }
            //if the node doesn't have right child, back to stack top, and visit right child
            if (!stack.isEmpty()){
                treeNode = stack.pop();
                System.out.println(treeNode.data);
                treeNode = treeNode.rightChild;
            }
        }
    }

    /**
     * post order traveral: left, right, root
     * @param root
     */
    public static void postOrderTraveral(TreeNode root){
        Stack<TreeNode> mainStack = new Stack<>();
        Stack<TreeNode> rightChildStack = new Stack<>();
        TreeNode treeNode = root;
        while (treeNode!=null || !mainStack.isEmpty()){
            //visit left child, and push into stack
            if (treeNode != null){
                if (treeNode.rightChild != null){
                    rightChildStack.push(treeNode.rightChild);
                }
                mainStack.push(treeNode);
                treeNode = treeNode.leftChild;
            }
            //if the node doesn't have right child, back to stack top, and visit right child
            else {
                treeNode = mainStack.peek();
                if (!rightChildStack.isEmpty() && rightChildStack.peek().equals(treeNode.rightChild)){
                    treeNode = rightChildStack.pop();
                }else{
                    System.out.println(treeNode.data);
                    mainStack.pop();
                    treeNode = null;
                }
            }
        }
    }

    /**
     * binary tree node
     */
    private static class TreeNode{
        int data;
        TreeNode leftChild;
        TreeNode rightChild;

        TreeNode(int data){
            this.data = data;
        }
    }

    public static void main(String[] args) {
        LinkedList<Integer> inputList = new LinkedList<Integer>(Arrays.asList(new Integer[]{3,2,9,null,null,10,null,null,8,null,4}));
        TreeNode treeNode = createBinaryTree(inputList);
        System.out.println("pre order:");
        preOrderTraveral(treeNode);
        System.out.println("in order");
        inOrderTraveral(treeNode);
        System.out.println("post order");
        postOrderTraveral(treeNode);
    }
}
