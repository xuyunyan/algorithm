package com.algorithm;

public class MyArray {
    //select time complexity: O(1)
    //update time complexity: O(1)
    private int[] array;
    private int size;

    public int[] getArray() {
        return array;
    }

    public int getSize() {
        return size;
    }

    public MyArray(int capacity){
        this.array = new int[capacity];
        size = 0;
    }
    /**
     * insert element
     * @param element
     * @param index
     * time complexity O(n)
     */
    public void insert(int element, int index) throws Exception{
        //check if over the range
        if (index<0 || index>size){
            throw new IndexOutOfBoundsException("Out of bounds!");
        }
        if (size >= array.length){
            resize();
        }
        for(int i=size-1; i>=index; i--){
            array[i+1] = array[i];
        }
        array[index] = element;
        size++;
    }

    /**
     * expend the size
     */
    public void resize(){
        int[] arrayNew = new int[array.length*2];
        //copy old array to new
        System.arraycopy(array,0, arrayNew, 0, array.length);
        array = arrayNew;
    }

    /**
     * delete element
     * @param index
     * Time complexity O(n)
     */
    public int delete(int index) throws Exception {
        if (index < 0 || index>=size){
            throw new IndexOutOfBoundsException("Out of bounds!");
        }
        int deletedElement = array[index];
        //move every element left 1 step
        for (int i=index; i<size; i++){
            array[i] = array[i+1];
        }
        size--;
        return deletedElement;
    }

    /**
     * print out the array
     */
    public void output(){
        for (int i=0; i<size; i++){
            System.out.println(array[i]);
        }
    }

}
