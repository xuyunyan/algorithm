package com.algorithm.hashmap;

import org.junit.Assert;
import org.junit.Test;

public class MyHashMapTest {

    @Test
    public void test(){
        MyHashMap<String, String> myHashMap = new MyHashMap<>();
        myHashMap.put("USA", "Washington DC");
        myHashMap.put("Nepal", "Kathmandu");
        myHashMap.put("India", "New Delhi");
        myHashMap.put("Australia", "Sydney");
        Assert.assertNotNull(myHashMap);
        Assert.assertEquals(16, myHashMap.getBucketSize());
        Assert.assertEquals("Kathmandu", myHashMap.get("Nepal"));
        Assert.assertEquals("Sydney", myHashMap.get("Australia"));
    }

    @Test
    public void testUpdate(){
        MyHashMap<String, String> myHashMap = new MyHashMap<>();
        myHashMap.put("USA", "Washington DC");
        myHashMap.put("Nepal", "Kathmandu");
        myHashMap.put("India", "New Delhi");
        myHashMap.put("Australia", "Sydney");
        myHashMap.put("USA", "NY");
        myHashMap.put("Australia", "M");
        Assert.assertEquals(4, myHashMap.getSize());
        Assert.assertEquals("NY", myHashMap.get("USA"));
        Assert.assertEquals("M", myHashMap.get("Australia"));
    }
}
