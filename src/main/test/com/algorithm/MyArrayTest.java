package com.algorithm;

import org.junit.Assert;
import org.junit.Test;

public class MyArrayTest {


    @Test
    public void testInsert() throws Exception {
        MyArray myArray = new MyArray(10);
        myArray.insert(3,0);
        myArray.insert(7,1);
        myArray.insert(9,2);
        myArray.insert(5,3);
        myArray.insert(6,1);
        myArray.output();
        Assert.assertArrayEquals(new int[]{3,6,7,9,5,0,0,0,0,0}, myArray.getArray());
    }

    @Test
    public void testResize() throws Exception {
        MyArray myArray = new MyArray(4);
        myArray.insert(3,0);
        myArray.insert(7,1);
        myArray.insert(9,2);
        myArray.insert(5,3);
        myArray.insert(6,1);
        myArray.output();
        Assert.assertArrayEquals(new int[]{3,6,7,9,5,0,0,0}, myArray.getArray());
    }

    @Test
    public void testDelete() throws Exception {
        MyArray myArray = new MyArray(10);
        myArray.insert(3,0);
        myArray.insert(7,1);
        myArray.insert(9,2);
        myArray.insert(5,3);
        myArray.insert(6,1);
        int deletedElement = myArray.delete(2);
        Assert.assertEquals(7, deletedElement);
        myArray.output();
        Assert.assertArrayEquals(new int[]{3,6,9,5,0,0,0,0,0,0}, myArray.getArray());
    }
}
