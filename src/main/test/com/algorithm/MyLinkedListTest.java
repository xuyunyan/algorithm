package com.algorithm;

import org.junit.Test;

public class MyLinkedListTest {

    @Test
    public void testInsertInOrder() throws Exception {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.insert(3,0);
        myLinkedList.insert(7,1);
        myLinkedList.insert(9,2);
        myLinkedList.insert(5,3);
        myLinkedList.output();
        //3 7 9 5 expected //Ocean: TODO: don't know how to write assert.....
    }

    @Test
    public void testInsertInMiddle() throws Exception {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.insert(3,0);
        myLinkedList.insert(7,1);
        myLinkedList.insert(9,2);
        myLinkedList.insert(5,3);
        myLinkedList.insert(6, 1);
        myLinkedList.output();
        //3 6 7 9 5 expected
    }

    @Test
    public void testRemove() throws Exception {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.insert(3,0);//insert head
        myLinkedList.insert(7,1);
        myLinkedList.insert(9,2);
        myLinkedList.insert(5,3);
        myLinkedList.insert(6, 1);
        myLinkedList.remove(4);//remove last one
        myLinkedList.output();
        //3 6 7 9  expected
    }

    @Test
    public void testRemoveInMiddle() throws Exception {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.insert(3,0);//insert head
        myLinkedList.insert(7,1);
        myLinkedList.insert(9,2);
        myLinkedList.insert(5,3);
        myLinkedList.insert(6, 1);
        myLinkedList.remove(2);//remove last one
        myLinkedList.output();
        //3 6 9 5 expected
    }
}
